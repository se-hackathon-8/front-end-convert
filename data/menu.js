const data = [
    {
      icon: "mdi-apps,",
      
      title: "Logo",
      to: "/",
    },
    {
      icon: "mdi-chart-bubble",
      title: "รายการโปรด",
      to: "/inspire",
    },
    {
        icon: "mdi-moon-waxing-crescent",
        title: "หน้าหลัก",
        to: "/6431503002"
    },
    {
        icon: "mdi-apple-icloud",
        title: "ราการเพิ่มเติม",
        to: "/6431503026"
    },
    {
        icon: "mdi-sun-compass",
        title: "ดาวน์โหลด",
        to: "/6431503042"
    },
    {
        icon: "mdi-apple-icloud",
        title: "ปักหมุด",
        to: "/6431503026"
    },
    {
        icon: "mdi-apple-icloud",
        title: "เวอร์ชั่น",
        to: "/6431503026"
    },
    {
        icon: "mdi-apple-icloud",
        title: "ออกจากระบบ",
        to: "/6431503026"
    },
  ]
  export default data;
